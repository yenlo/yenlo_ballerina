import ballerina.net.http;
import ballerina.net.jms;
import ballerina.lang.exceptions;
import ballerina.lang.messages;
import ballerina.lang.system;
import ballerina.lang.strings;
import ballerina.lang.jsons;
import ballerina.lang.xmls;
import ballerina.utils;

@http:BasePath("/count")
service YenloCarsCountService {
	http:ClientConnector carsService = create http:ClientConnector(
	    "https://yenlo-cars.herokuapp.com");
	
	@http:GET
	@http:Path("/")
	resource count(message m) {
		message response = {};
		try {
			 response = http:ClientConnector.get(carsService, 
			    "/count", updateHeader(m));
			 send(response);
		}
		catch (exception e) {
			system:log(5, 
			    exceptions:getCategory(e) + " : " + exceptions:getMessage(e));
		}
		reply response;
	}
	
	@http:GET
	@http:Path("/marks")
	resource countMarks(message m) {
		message response = {};
		try {
			 response = http:ClientConnector.get(carsService, 
			    "/count/marks", updateHeader(m));
			 send(response);
		}
		catch (exception e) {
			system:log(5,
			    exceptions:getCategory(e) + " : " + exceptions:getMessage(e));
		}
		reply response;
	}
	
	@http:GET
	@http:Path("/marks/{mark}")
	resource countMark(message m, @http:PathParam("mark")string mark) {
		message response = {};
		try {
			 response = http:ClientConnector.get(carsService, 
			    "/count/mark/" + mark, updateHeader(m));
			 send(response);
		}
		catch (exception e) {
			system:log(5,
			    exceptions:getCategory(e) + " : " + exceptions:getMessage(e));
		}
		reply response;
	}
}

function updateHeader(message m)(message ) {
	string 
	type =  "";
	if ((int )(system:nanoTime() % 2) == 0) {
		type =  "application/json";
	}
	else {
		type =  "application/xml";
	}
	messages:addHeader(m, "Content-Type", type);
	messages:addHeader(m, "Accept", type);
	messages:addHeader(m, "Authorization", 
	    "Basic " + utils:base64encode("yenlo-cars:yenlo-cars-demo"));
	return m;
}

function send(message m)throws exception {
	map propertyMap = {};
	message msg = {};
	string value;
	try {
		if (strings:contains(messages:getHeader(m, "Content-Type"), "application/json")) {
			 value = jsons:toString(messages:getJsonPayload(m));
		}
		else {
			 value = xmls:toString(messages:getXmlPayload(m));
		}
		messages:setStringPayload(msg, value);
		jms:ClientConnector jmsAMQ = create jms:ClientConnector(
		    "org.apache.activemq.jndi.ActiveMQInitialContextFactory", 
		    "tcp://localhost:61616");
		jms:ClientConnector.send(jmsAMQ, "QueueConnectionFactory", 
		    "ballerina.cars.http", "queue", "TextMessage", msg, propertyMap);
	}
	catch (exception e) {
		throw e;
	}
}
