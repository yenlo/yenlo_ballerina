import ballerina.data.sql;
import ballerina.net.jms;
import ballerina.lang.datatables;
import ballerina.lang.exceptions;
import ballerina.lang.messages;
import ballerina.lang.jsons;
import ballerina.lang.system;

@http:BasePath("/count")
service YenloCarsDBService {
	map connection = {
	    "driverClassName":"com.mysql.jdbc.Driver", 
	    "jdbcUrl":"jdbc:mysql://onnjomlc4vqc55fw.chr7pe7iynqr.eu-west-1.rds.amazonaws.com:3306/kfnda31gr0xld8tx", 
	    "username":"sb8ane8p28cycgwb", 
	    "password":"ft2rtycukmck6fol"
	};
	sql:ClientConnector carsDB = create sql:ClientConnector(connection);
	
	@http:GET
	@http:Path("/")
	resource count(message m) {
		sql:Parameter[] params = [];
		message response = {};
		try {
			 response = process(carsDB, "select count(1) as records from car", params);
		}
		catch (exception e) {
			system:log(5, exceptions:getCategory(e) + " : " + exceptions:getMessage(e));
		}
		reply response;
	}
	
	@http:GET
	@http:Path("/marks")
	resource countMarks(message m) {
		sql:Parameter[] params = [];
		message response = {};
		try {
			 response = process(carsDB, 
			    "select distinct(mark), count(1) as records from car group by mark", params);
		}
		catch (exception e) {
			system:log(5, exceptions:getCategory(e) + " : " + exceptions:getMessage(e));
		}
		reply response;
	}
	
	@http:GET
	@http:Path("/marks/{mark}")
	resource countMark(message m, 
	@http:PathParam("mark")string mark) {
		sql:Parameter param = {sqlType:"varchar", value:mark, direction:0};
		sql:Parameter[] params = [param];
		message response = {};
		try {
			 response = process(carsDB, 
			    "select mark, count(1) as records from car where mark = ?", params);
		}
		catch (exception e) {
			system:log(5, exceptions:getCategory(e) + " : " + exceptions:getMessage(e));
		}
		reply response;
	}
}

function process(sql:ClientConnector cc, string query, sql:Parameter[] params)(message ) throws exception {
    datatable dt = sql:ClientConnector.select(cc, query, params);
	message msg = {};
	try {
		json records = datatables:toJson(dt);
		messages:setJsonPayload(msg, records);
		send(jsons:toString(records));
	}
	catch (exception e) {
	    throw e;
	}
	datatables:close(dt);
	return msg;
}

function send(string text) throws exception {
	map propertyMap = {};
	message msg = {};
	try {
	    messages:setStringPayload(msg, text);
	    jms:ClientConnector jmsAMQ = create jms:ClientConnector(
	        "org.apache.activemq.jndi.ActiveMQInitialContextFactory", 
	        "tcp://localhost:61616");
	    jms:ClientConnector.send(jmsAMQ, "QueueConnectionFactory", 
	        "ballerina.cars.sql", "queue", "TextMessage", 
	        msg, propertyMap);
	}
	catch (exception e) {
	    throw e;
	}
}
