import ballerina.net.jms;

@Source (
    protocol = "jms", 
    destination = "ballerina.cars.*", 
    connectionFactoryJNDIName = "QueueConnectionFactory",
    factoryInitial = "org.apache.activemq.jndi.ActiveMQInitialContextFactory", 
    providerUrl = "tcp://localhost:61616",
    connectionFactoryType = "queue")
service ActiveMQQueueService {
    resource onMessage (message m) {
        map propertyMap = {}; 
        jms:ClientConnector jmsAMQ = create jms:ClientConnector(
            "org.apache.activemq.jndi.ActiveMQInitialContextFactory", 
            "tcp://localhost:61616");
        jms:ClientConnector.send(jmsAMQ, "TopicConnectionFactory", 
            "ballerina.cars", "topic", jms:getMessageType(m), m, propertyMap);
    }
}