package com.yenlo.compare;

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class Ballerina extends Simulation {
  val count = http("count").get("/count");
  val marks = http("marks").get("/count/marks");
  val countHonda = http("count-honda").get("/count/marks/HONDA");
  
  val scn = scenario("ballerina").repeat(5) {
    exec(count).pause(5).
    exec(marks).pause(5).
    exec(countHonda).pause(5);
  }
  
  val httpConf = http.baseURL("http://localhost:9090");
  setUp(
    scn.inject(atOnceUsers(10))
  ).protocols(httpConf);
}
