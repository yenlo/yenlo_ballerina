package com.yenlo.compare;

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class ServiceHeroku extends Simulation {
  val map = Map(
      "Authorization"->"Basic eWVubG8tY2Fyczp5ZW5sby1jYXJzLWRlbW8=");
  val json = map + ("Content-Type"->"application/json", "Accept"->"application/json");
  val xml = map + ("Content-Type"->"application/xml", "Accept"->"application/xml");
  
  val count = http("count").get("/count");
  val marks = http("marks").get("/count/marks");
  val countHonda = http("count-honda").get("/count/mark/HONDA");
  
  val scnJSON = scenario("json").repeat(5) {
    exec(count.headers(json)).pause(5).
    exec(marks.headers(json)).pause(5).
    exec(countHonda.headers(json)).pause(5);
  }
  
  val scnXML = scenario("xml").repeat(5) {
    exec(count.headers(xml)).pause(5).
    exec(marks.headers(xml)).pause(5).
    exec(countHonda.headers(xml)).pause(5);
  }

  val httpConf = http.baseURL("https://yenlo-cars.herokuapp.com");
  setUp(
    scnJSON.inject(atOnceUsers(5)),
    scnXML.inject(atOnceUsers(5))
  ).protocols(httpConf);
}
