![Yenlo](http://mms.businesswire.com/media/20160329005629/en/516023/21/Yenlo_logo_square.jpg)

# ![Ballerina](http://ballerinalang.org/img/ballerina-logo.svg)
During the [WSO2 US Conference 2017](http://wso2.com/library/conference/2017/2/wso2con-usa-2017-introducing-ballerina/) the new programming language for WSO2 products that use mediation and transformation was announced called Ballerina.

What is Ballerina? It is an open-source project initiated by WSO2 to foster the development of the programming language called Ballerina and its supporting technologies like IDEs, Debuggers, Testing-Frameworks and so on. Ballerina is not a product but it will be used in the future as component within products. Although WSO2 is the initiator and main sponsor of the development of Ballerina everybody is free to join and contribute. Right now it is a technology preview currently in version 0.8.3. This means that before we get to version 1.0.0 things might change a bit (planned May 2017). 

## Goal
Demonstration to write the services in WSO2 Ballerina service. 

As a test environment used stress tool software [gatling.io](http://gatling.io).
## Requirements
* Install [WSO2 Ballerina](http://ballerinalang.org/downloads/ballerina-runtime/ballerina-0.8.3.zip).
* Yenlo Cars API which can be build and run on you environment (local computer) or using Heroku application servers.
  * Local
    * download project [Yenlo Cars API](https://bitbucket.org/yenlo/yenlo_cars)
    * upload schema and data to MySQL
    ```sh
    mysql> source yenlo-cars-import/sql/schema.sql;
    mysql> source yenlo-cars-import/sql/cars-backup.sql;
    ```
    * build project (Java8 required) under yenlo-cars directory
    ```sh
    $ mvn clean install
    ```
    * run server
    ```sh
    $ java -jar yenlo-cars-api/target/dependency/webapp-runner.jar --path /cars --port 8080 yenlo-cars-api/target/*.war
    ```
  * Heroku
    * https://yenlo-cars.herokuapp.com - nothing to do (installed and running)

## Run
### Ballerina
```sh
# run HTTP service
$ $BALLERINA_HOME/bin/ballerina run service src/main/ballerina/com/yenlo/http/YenloCarsCountService.bal
# run JMS service
$ $BALLERINA_HOME/bin/ballerina run service src/main/ballerina/com/yenlo/jms/JMSService.bal
# run SQL service
$ $BALLERINA_HOME/bin/ballerina run service src/main/ballerina/com/yenlo/sql/YenloCarsDBService.bal
```
The running service uses port 9090 (by default), in this case only one service can be run.
### Tests
```sh
# run tests calling Ballerina service
$ mvn gatling:execute -Dgatling.simulationClass=com.yenlo.compare.Ballerina
# run tests for REST API on local machine
$ mvn gatling:execute -Dgatling.simulationClass=com.yenlo.compare.ServiceLocalhost
# run tests for REST API on Heroku
$ mvn gatling:execute -Dgatling.simulationClass=com.yenlo.compare.ServiceHeroku
```